const { default: slugify } = require("slugify");

const slug = (name) =>
  slugify(name, { replacement: "+", lower: true, trim: true });

module.exports = slug;

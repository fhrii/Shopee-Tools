function getShopIdAndItemId(link) {
  const [, shopId, itemId] = link.match(/-i(.)[0-9]+(.)[0-9]+/g)[0].split(".");

  return { shopId, itemId };
}

function getShopIdAndItemIdByLink(link) {
  if (Array.isArray(link)) return link.map(getShopIdAndItemId);

  return getShopIdAndItemId(link);
}

module.exports = getShopIdAndItemIdByLink;

const autoScroll = require("./autoScroll");
const getShopIdAndItemIdByLink = require("./getShopIdAndItemIdByLink");
const log = require("./log");
const makeTimeout = require("./makeTimeout");
const slug = require("./slug");

module.exports = {
  autoScroll,
  log,
  slug,
  getShopIdAndItemIdByLink,
  makeTimeout,
};

function makeTimeout(time = 2000) {
  return new Promise((r) => setTimeout(r, time));
}

module.exports = makeTimeout;

const { select } = require("@inquirer/prompts");
const puppeteer = require("puppeteer");
const { addExtra } = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");

const {
  GetUserOnSpecificProductsReview,
  GetReviewByProduct,
  BulkMessageToUsers,
  MergeUsers,
} = require("./tools");

async function main() {
  const ptpr = addExtra(puppeteer);
  ptpr.use(StealthPlugin());

  const bulkMessageToUsers = new BulkMessageToUsers(ptpr);
  const getReviewByProduct = new GetReviewByProduct();
  const getUserOnSpecificProductsReview = new GetUserOnSpecificProductsReview(
    ptpr,
    getReviewByProduct
  );
  const mergeUsers = new MergeUsers();

  const tool = await select({
    message: "Pilih tool yang ingin digunakan",
    choices: [
      {
        name: "1. Mendapatkan user yang sudah mereviu produk dengan keyword tertentu",
        value: GetUserOnSpecificProductsReview.name,
      },
      {
        name: "2. Kirim pesan ke banyak pengguna",
        value: BulkMessageToUsers.name,
      },
      {
        name: "3. Gabungkan banyak pengguna",
        value: MergeUsers.name,
      },
    ],
  });

  switch (tool) {
    case GetUserOnSpecificProductsReview.name:
      getUserOnSpecificProductsReview.execute();
      break;
    case BulkMessageToUsers.name:
      bulkMessageToUsers.execute();
      break;
    case MergeUsers.name:
      mergeUsers.execute();
      break;
    default:
      console.log("Tool not yet supported");
  }
}

main();

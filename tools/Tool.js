class Tool {
  constructor() {
    if (this.constructor === Tool)
      throw new Error("Abstract classes can' be instantiated");
  }

  execute() {
    throw new Error("Method 'execute' must be implemented");
  }
}

module.exports = Tool;

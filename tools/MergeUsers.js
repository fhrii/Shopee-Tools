const { select, confirm, checkbox, input } = require("@inquirer/prompts");
const fs = require("fs-extra");

const Tool = require("./Tool");
const { log, slug } = require("../utils");
const chalk = require("chalk");

class MergeUsers extends Tool {
  async execute() {
    const usersBasePath = "./data/users";
    const isUserBasePath = await fs.pathExists(usersBasePath);

    if (!isUserBasePath) {
      log(chalk.bold("Pastikan folder pengguna ada"));
      return;
    }

    const rawListUserFiles = await fs.readdir(usersBasePath);
    const listUserFiles = rawListUserFiles.filter((fn) => fn.includes(".json"));

    if (listUserFiles.length === 0) {
      log(chalk.bold("Pastikan sudah melakukan pengambilan data"));
      return;
    }

    const selectedUserFileName = await checkbox({
      message: "Pilih pengguna yang ingin Anda gabungkan",
      choices: listUserFiles.map((fn) => ({ value: fn, name: fn })),
    });

    if (selectedUserFileName.length <= 1) {
      log(chalk.bold("Anda tidak menggabungkan pengguna apapun"));
      return;
    }

    const shouldMerging = await confirm({
      message: `Apakah Anda yakin ingin menggabungkan?`,
    });
    const rawFileName = await input({
      message: "Berikan nama file untuk hasil gabungan penguna",
    });
    const fileName = `${Date.now()}-${slug(rawFileName)}`;

    if (!shouldMerging) {
      log(chalk.bold("Penggabungan dibatalkan"));
      return;
    }

    const userFileJsons = await Promise.all(
      selectedUserFileName.map((sufn) =>
        fs.readJson(`${usersBasePath}/${sufn}`)
      )
    );
    const keyword = userFileJsons.map((ufj) => ufj.keyword).join("-");
    const rawUsers = userFileJsons.flatMap((ufj) => ufj.users);
    const users = rawUsers.filter(
      (ru, index) =>
        rawUsers.findIndex((ru2) => ru.shopId === ru2.shopId) === index
    );

    await fs.writeJSON(`${usersBasePath}/${fileName}.json`, {
      keyword,
      createdAt: new Date().toLocaleString(),
      latestExecuted: null,
      totalUsers: users.length,
      users,
    });

    log(chalk.bold(`Penggabungan berhasil dilakukan (${fileName}.json)`));
  }
}

module.exports = MergeUsers;

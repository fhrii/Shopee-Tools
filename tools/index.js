const GetUserOnSpecificProductsReview = require("./GetUserOnSpecificProductsReview");
const GetReviewByProduct = require("./GetReviewByProduct");
const BulkMessageToUsers = require("./BulkMessageToUsers");
const MergeUsers = require("./MergeUsers");

module.exports = {
  BulkMessageToUsers,
  GetUserOnSpecificProductsReview,
  GetReviewByProduct,
  MergeUsers,
};

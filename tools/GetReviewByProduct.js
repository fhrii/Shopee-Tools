const axios = require("axios");
const { PromisePool } = require("@supercharge/promise-pool");

const { getShopIdAndItemIdByLink, log } = require("../utils");
const Tool = require("./Tool");

class GetReviewByProduct extends Tool {
  async execute(link) {
    const limit = 50;
    const { shopId, itemId } = getShopIdAndItemIdByLink(link);
    const baseUrlReview = `https://shopee.co.id/api/v2/item/get_ratings?itemid=${itemId}&shopid=${shopId}`;
    const ratingResponse = await axios.get(`${baseUrlReview}&type=0`);
    const totalReviewPerRating =
      ratingResponse.data.data.item_rating_summary.rating_count;
    const reviewUrlList = totalReviewPerRating.flatMap((val, index) => {
      const totalPage = Math.ceil(val / 50);
      const allOffset = Array(totalPage > 6000 ? 6000 : totalPage)
        .fill("")
        .map((_, i) => i * limit);

      return allOffset.map((offset) =>
        axios.get(
          `${baseUrlReview}&type=${index + 1}&limit=${limit}&offset=${offset}`
        )
      );
    });
    const { results } = await PromisePool.for(reviewUrlList)
      .withConcurrency(2)
      .handleError((error, data) => {
        if (error) log({ error, data });
      })
      .process((response) => {
        return response.data.data.ratings || [];
      });

    return results.flatMap((result) => result);
  }
}

module.exports = GetReviewByProduct;

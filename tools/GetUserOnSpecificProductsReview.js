const { confirm, input, select } = require("@inquirer/prompts");
const fs = require("fs-extra");
const { PendingXHR } = require("pending-xhr-puppeteer");
const loading = require("loading-cli");
const chalk = require("chalk");

const Tool = require("./Tool");
const { autoScroll, slug, log } = require("../utils");
const cookies = require("../data/shopee.json");

class GetUserOnSpecificProductsReview extends Tool {
  constructor(puppeteer, getReviewByProduct) {
    super();

    this.puppeteer = puppeteer;
    this.getReviewByProduct = getReviewByProduct;
  }

  async execute() {
    const isAlreadySearch = await confirm({
      message: "Apakah sudah melakukan pencarian?",
    });
    const load = loading({
      text: chalk.bold("Executing..."),
      interval: 100,
      stream: process.stdout,
      frames: ["◰", "◳", "◲", "◱"],
    });
    let keyword;
    let filePath;

    if (!isAlreadySearch) {
      const rawKeyword = await input({
        message: "Masukan produk yang ingin dicari",
      });
      const rawFileName = await input({
        message: "Berikan nama file untuk produk yang dicari",
      });
      const fileName = `${Date.now()}-${slug(rawFileName)}`;
      const keywordArray = rawKeyword.split(",").map((rk) => slug(rk));
      keyword = keywordArray.join("-");
      filePath = `./data/products/${fileName}.json`;
      const maxPage = parseInt(process.env.MAX_PAGE) || 17;

      load.start();
      await fs.remove(filePath);

      const browser = await this.puppeteer.launch({
        headless: process.env.SHOW_BROWSER !== "true" ? "new" : false,
        args: ["--start-maximized"],
        args: [`--window-size=1920,1080`],
      });
      const page = await browser.newPage();
      const pendingXHR = new PendingXHR(page);

      await page.setCookie(...cookies);

      for (const keywordIndex in keywordArray) {
        for (let i = 1; i <= maxPage; i++) {
          const currentKeyword = keywordArray[keywordIndex];
          load.text = chalk.bold(
            `Executing... (${currentKeyword} (${+keywordIndex + 1}/${
              keywordArray.length
            }) - Page ${i}/${maxPage})`
          );
          const currentPage = i > 1 ? `&page=${i}` : "";
          await page.goto(
            `https://shopee.co.id/search?keyword=${currentKeyword}` +
              currentPage,
            {
              waitUntil: "networkidle0",
            }
          );

          const emptyResultElp = await page.$(
            ".shopee-search-empty-result-section__title"
          );

          if (emptyResultElp) {
            log("Selesai, tidak ada lagi produk");
            break;
          }

          await autoScroll(page);
          await pendingXHR.waitForAllXhrFinished();

          const products = await page.evaluate(() => {
            return [
              ...document.querySelectorAll(
                ".shopee-search-item-result__item > a"
              ),
            ].map((aEl) => "https://shopee.co.id" + aEl.getAttribute("href"));
          });
          const filteredProducts = products.filter(
            (nP, index) => products.indexOf(nP) === index
          );

          const isFileExists = await fs.exists(filePath);

          if (isFileExists) {
            const productJson = await fs.readJson(filePath);
            const newProducts = [...productJson.products, ...filteredProducts];
            const filteredNewProducts = newProducts.filter(
              (nP, index) => newProducts.indexOf(nP) === index
            );

            productJson.products = filteredNewProducts;
            productJson.totalProducts = filteredNewProducts.length;
            await fs.writeJson(filePath, productJson);
            continue;
          }

          await fs.outputFile(
            filePath,
            JSON.stringify({
              keyword,
              createdAt: new Date().toLocaleString(),
              latestExecuted: null,
              totalProducts: products.length,
              products,
            })
          );
        }
      }

      load.stop();
      browser.close();
      log(chalk.bold("Berhasil mendapatkan data produk"));
    } else {
      const productsBasePath = "./data/products";
      const isProductBasePathExists = await fs.pathExists(productsBasePath);

      if (!isProductBasePathExists) {
        log(chalk.bold("Pastikan folder produk ada"));
        return;
      }

      const rawListProductFiles = await fs.readdir(productsBasePath);
      const listProductFiles = rawListProductFiles.filter((fn) =>
        fn.includes(".json")
      );

      if (listProductFiles.length === 0) {
        log(chalk.bold("Pastikan sudah melakukan pengambilan data"));
        return;
      }

      keyword = await select({
        message: "Pilih pengguna pada produk yang ingin dicari",
        choices: listProductFiles.map((lpf) => ({ value: lpf, name: lpf })),
      });
      keyword = keyword.replace(".json", "");
      filePath = `./data/products/${keyword}.json`;
    }

    const authorFilePath = `./data/users/${keyword}.json`;
    const productJson = await fs.readJson(filePath);
    let counter = productJson.latestExecuted ?? -1;

    await fs.remove(authorFilePath);
    load.text = "Executing...";
    load.start();

    while (
      productJson.totalProducts > 0 &&
      productJson.totalProducts - 1 > counter
    ) {
      counter += 1;
      load.text = `Executing (${counter + 1}/${productJson.totalProducts})`;

      const productReviews = await this.getReviewByProduct.execute(
        productJson.products[counter]
      );
      const authors = productReviews.map((productReview) => ({
        shopId: productReview.author_shopid,
        username: productReview.author_username,
      }));
      const filteredAuthors = authors.filter(
        (author, index) =>
          authors.findIndex((a) => a.shopId === author.shopId) === index
      );

      const pJson = await fs.readJson(filePath);

      pJson.latestExecuted = counter;
      await fs.writeJson(filePath, pJson);

      const isAuthorFileExists = await fs.exists(authorFilePath);

      if (isAuthorFileExists) {
        const authorJson = await fs.readJson(authorFilePath);
        const newAuthors = [...authorJson.users, ...filteredAuthors];
        const filteredNewAuthors = newAuthors.filter(
          (author, index) =>
            newAuthors.findIndex((a) => a.shopId === author.shopId) === index
        );

        authorJson.users = filteredNewAuthors;
        authorJson.totalUsers = filteredNewAuthors.length;
        await fs.writeJson(authorFilePath, authorJson);
        continue;
      }

      await fs.outputFile(
        authorFilePath,
        JSON.stringify({
          keyword,
          createdAt: new Date().toLocaleString(),
          latestExecuted: null,
          totalUsers: authors.length,
          users: authors,
        })
      );
    }

    load.stop();
    log(chalk.bold("Berhasil mendapatkan data pengguna"));
  }
}

module.exports = GetUserOnSpecificProductsReview;

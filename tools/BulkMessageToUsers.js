const fs = require("fs-extra");
const chalk = require("chalk");
const { select } = require("@inquirer/prompts");
const loading = require("loading-cli");
const { PendingXHR } = require("pending-xhr-puppeteer");

const Tool = require("./Tool");
const { log, makeTimeout } = require("../utils");
const cookies = require("../data/shopee.json");

class BulkMessageToUsers extends Tool {
  constructor(puppeteer) {
    super();

    this.puppeteer = puppeteer;
  }

  async execute() {
    const messagesBasePath = "./data/messages";
    const usersBasePath = "./data/users";
    const [isMessageBasePathExists, isUserBasePath] = await Promise.all([
      fs.pathExists(messagesBasePath),
      fs.pathExists(usersBasePath),
    ]);

    if (!isMessageBasePathExists || !isUserBasePath) {
      log(chalk.bold("Pastikan folder template pesan dan pengguna ada"));
      return;
    }

    const [rawListMessageFiles, rawListUserFiles] = await Promise.all([
      fs.readdir(messagesBasePath),
      fs.readdir(usersBasePath),
    ]);
    const [listMessageFiles, listUserFiles] = [
      rawListMessageFiles.filter((fn) => fn.includes(".txt")),
      rawListUserFiles.filter((fn) => fn.includes(".json")),
    ];

    if (listMessageFiles.length === 0) {
      log(chalk.bold("Buat template pesan terlebih dahulu"));
      return;
    }

    if (listUserFiles.length === 0) {
      log(chalk.bold("Pastikan sudah melakukan pengambilan data"));
      return;
    }

    const messageFileName = await select({
      message: "Pilih template pesan yang ingin digunakan",
      choices: listMessageFiles.map((fn) => ({ value: fn, name: fn })),
    });
    const userFileName = await select({
      message: "Pilih pengguna yang ingin dikirimkan pesan",
      choices: listUserFiles.map((fn) => ({ value: fn, name: fn })),
    });
    const messageFilePath = messagesBasePath + "/" + messageFileName;
    const userFilePath = usersBasePath + "/" + userFileName;
    const [message, userJson] = await Promise.all([
      fs.readFile(messageFilePath, {
        encoding: "utf-8",
      }),
      fs.readJson(userFilePath),
    ]);
    const messageList = message.split("<--ENTER-->");
    const load = loading({
      text: chalk.bold("Executing..."),
      interval: 100,
      stream: process.stdout,
      frames: ["◰", "◳", "◲", "◱"],
    }).start();
    let counter = userJson.latestExecuted ?? -1;
    let totalPrivateUser = 0;
    let totalSuccess = 0;
    const isFinishAlready = userJson.totalUsers === counter + 1;

    if (isFinishAlready) {
      load.stop();
      log(
        chalk.bold(
          `Semua pengguna sudah di-chat, ubah ${chalk.italic(
            "latestExecuted"
          )} menjadi ${chalk.italic("null")} apabila ingin mengulang kembali`
        )
      );
      return;
    }

    const browser = await this.puppeteer.launch({
      headless: process.env.SHOW_BROWSER !== "true" ? "new" : false,
      args: ["--start-maximized"],
      args: [`--window-size=1920,1080`],
    });
    const page = await browser.newPage();
    const pendingXHR = new PendingXHR(page);

    await page.setCookie(...cookies);

    while (userJson.totalUsers > 0 && userJson.totalUsers - 1 > counter) {
      counter += 1;
      load.text = chalk.bold(
        `Executing...(${counter}/${userJson.totalUsers}) - Disable Chat: ${totalPrivateUser} - Success: ${totalSuccess}`
      );

      await page.goto(
        `https://shopee.co.id/shop/${userJson.users[counter].shopId}`,
        {
          waitUntil: "networkidle0",
        }
      );
      const isUsernameExists = await page.evaluate(() => {
        return Boolean(document.querySelector(".navbar__username"));
      });

      if (!isUsernameExists) {
        load.stop();
        log(chalk.bold("Mohon login untuk menggunakan fitur ini"));
        break;
      }

      const chatButtonSelector = "a[argettype='chatButton'] > button";
      const chatButton = await page.$(chatButtonSelector);

      if (!chatButton) {
        totalPrivateUser += 1;
        continue;
      }

      chatButton.evaluate((b) => b.click());

      const chatInputSelector =
        "textarea[data-cy='minichat-input-editor-input']";

      await page.waitForSelector(chatInputSelector);

      const inputText = await page.$(chatInputSelector);

      for (const m of messageList) {
        await inputText.evaluate((it, t) => {
          it.value = t;
        }, m.trim());
        await page.focus(chatInputSelector);
        await page.keyboard.type(" \n");
        await pendingXHR.waitForAllXhrFinished(page);
        await makeTimeout(process.env.DELAY_BETWEEN_CHAT);
      }

      const uJson = await fs.readJson(userFilePath);

      uJson.latestExecuted = counter;
      await fs.writeJson(userFilePath, uJson);

      if (userJson.totalUsers - 1 > counter)
        await makeTimeout(process.env.DELAY_BETWEEN_USER);

      totalSuccess += 1;
    }

    load.stop();

    if (!isFinishAlready)
      log(
        chalk.bold(
          `Berhasil mengirimkan pesan ke pengguna\nSuccess: ${totalSuccess}\nDisable Chat: ${totalPrivateUser}`
        )
      );

    await browser.close();
  }
}

module.exports = BulkMessageToUsers;
